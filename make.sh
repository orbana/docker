#!/bin/bash

SRC="./src"
DOCKER_GROUP="p92docker"
IMAGES=(
    "ubuntu-base"
    "ubuntu-java-oracle"
    "ubuntu-java-oracle-tomcat"
    "ubuntu-liferay"
)

function help_menu() {
cat << EOF
Usage: ${0} (-h|-a|-b|-c|--clean-all)

OPTIONS:
    -h|--help           Show this message
    -a|--all            Build all
    -b|--build          Build image [image-name]
    -c|--clean          Clean image [image-name]
    --clean-all         Clean all

IMAGES:
    ${IMAGES[@]}

EOF
}

function build() {
    echo "Build ${1}..."
    docker build -t ${DOCKER_GROUP}/${1} -f ${SRC}/${1}/Dockerfile .
}

function build_all() {
    echo "Build all..."
    for image in "${IMAGES[@]}"
    do
        build "${image}"
    done
}

function clean() {
    echo "Clean ${1}..."
    docker rmi ${DOCKER_GROUP}/${1}
}

function clean_all() {
    echo "Clean all..."
    for image in "${IMAGES[@]}"
    do
        clean "${image}"
    done
}

while [[ $# > 0 ]]
do
case "${1}" in
  -h|--help)
  help_menu
  shift
  ;;
  -a|--all)
  build_all
  shift
  ;;
  -b|--build)
  build "${2:-${IMAGE_NAME}}"
  shift
  ;;
  -c|--clean)
  clean "${2:-${IMAGE_NAME}}"
  shift
  ;;
  --clean-all)
  clean_all
  shift
  ;;
  *)
  echo "${1} is not a valid flag, try running: ${0} --help"
  ;;
esac
shift
done
