# Docker files

### Installation

1. Install [Docker](https://www.docker.com/).
2. Run the ./make.sh script.

### Uninstall
1. sudo apt-get purge -y docker-engine
2. sudo apt-get autoremove -y --purge docker-engine
3. sudo apt-get autoclean
4. sudo rm -rf /var/lib/docker

### Help
    ./make.sh -h

### Create images
    ./make.sh -a

### Remove images
    ./make.sh --clean-all
    
### Usage

##### Base Ubuntu 
    docker run --rm -it p92docker/ubuntu-base /bin/bash
##### Base Ubuntu with Oracle Java 
    docker run --rm -it p92docker/ubuntu-java-oracle /bin/bash
##### Tomcat with Oracle Java 
    docker run --rm -it p92docker/ubuntu-java-oracle-tomcat /bin/bash
##### Liferay with Oracle Java 
    docker run --rm -it p92docker/ubuntu-liferay /bin/bash

##### Helper page for Apache verification
    https://www.apache.org/info/verification.html

#### Issues

##### Error creating default "bridge" network: failed to allocate gateway...

There is a bug in the network database. Remove the /var/lib/docker/network/files/local-kv.db file, and rebuild the images

##### ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?

$ sudo service docker start
